﻿Folders:

Commands
- folder that uses the command pattern principle. commands are classes that have a common interface called execute(). In that way, commands can be chained and executed all at once or queued.

ErrorCheck
- provides a basic logger that is stored in the main app ./bin/
- a custom Exception for an out of bounds Rover

Movement
- Coordinates, Direction, Vector Operations and Orientation of Rovers

Command Mapper is a 'parser' that will validate an input block
The Rover builder uses the mapper to create the Rovers list
Rover.cs - contains the class definition of a Rover
