﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    /// <summary>
    /// Interface for the Rover Class
    /// </summary>
    interface IRoverCommand
    {
        void Move(); 
        void RotateLeft();
        void RotateRight();
        void RunCommands();
    }
}
