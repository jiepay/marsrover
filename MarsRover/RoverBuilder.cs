﻿using MarsRover.ErrorCheck;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    /// <summary>
    /// The Rover Builder
    /// </summary>
    public class RoverBuilder
    {
        public string MissionName { get; private set; }
        public MarsPlateau planetMarsGrid { get; set; }
        public List<Rover> Rovers { get; set; }

        public RoverBuilder(string missionName, MarsPlateau planetMarsGrid)
        {
            this.MissionName = missionName;
            this.planetMarsGrid = planetMarsGrid;
        }

        public RoverBuilder(string missionName, MarsPlateau planetMarsGrid,
               List<Rover> rovers)
        {
            this.MissionName = missionName;
            this.planetMarsGrid = planetMarsGrid;
            this.Rovers = rovers;
        }

        /// <summary>
        /// Gets the Data from the parser and Executes All Rover Commands one at a time for each rover
        /// </summary>
        public void RunCommandMapper()
        {
            if (Rovers == null || Rovers.Count == 0)
                return;

            try
            {
                foreach(var r in Rovers)
                {
                    r.PlateauUpperBound = MarsPlateau.Upperbound;
                    r.RunCommands();                   
                    Console.WriteLine(r.ActiveLocation + " " + r.CurrentDirection);//TODO: Remove, put here only for the purpose of the assignment
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
        }
    }
}
