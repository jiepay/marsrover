﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover.Commands
{
    /*
     * Code the commands to a generic interface with an execute function 
     * which will allow us to detach the commands implementation from the object using them 
     * this can allow re-use, queuing and chain execution (Command Pattern)
    */
    public interface ICommand
    {
        void Execute(Rover r);
    }
}
