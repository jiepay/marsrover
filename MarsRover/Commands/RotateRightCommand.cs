﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover.Commands
{
    public class RotateRightCommand : ICommand
    {
        public void Execute(Rover r)
        {
            r.RotateRight();
        }
    }
}
