﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover.Commands
{
    public class MoveCommand : ICommand
    {
        public void Execute(Rover r)
        {
            r.Move();
        }
    }
}
