﻿using System;
using System.Collections.Generic;
using System.Text;
using MarsRover.Movement;

namespace MarsRover
{
    public interface ICommandMapper
    {
        bool ValidateCommandSet(string commandString);
    }
}
