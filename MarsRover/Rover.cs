﻿using MarsRover.Commands;
using MarsRover.ErrorCheck;
using MarsRover.Movement;
using System;
using System.Collections.Generic;

namespace MarsRover
{
    /// <summary>
    /// The Rover class
    /// </summary>
    public class Rover : IRoverCommand
    {
        public string RoverName { get; private set; }
        private static int defaultGridMovement = 1;

        public Coordinates ActiveLocation { get; private set; }
        public Direction CurrentDirection { get; private set; }
        public List<ICommand> RoverCommandsList { get; set; }
        public Coordinates PlateauUpperBound { get; set; }
        
        /// <summary>
        /// Rover Constructor
        /// </summary>
        /// <param name="roverName">The Rover Name</param>
        /// <param name="coordinates">The Rover Coordinates</param>
        /// <param name="currentDirection">The Current Direction the rover is facing</param>
        public Rover(string roverName, Coordinates coordinates,
            Direction currentDirection)
        {
            this.RoverName = roverName;
            this.ActiveLocation = coordinates;
            this.CurrentDirection = currentDirection;
        }

        /// <summary>
        /// Basic movement for the rover
        /// </summary>
        public void Move()
        {
            var tranformation = this.ActiveLocation.MoveTransform(
                this.CurrentDirection.CurrentOrientation,
                defaultGridMovement);

            //is object inside plateau after a move attempt
            if (MarsPlateau.IsObjectInsidePlateau(this.ActiveLocation + tranformation))
            {
                //if yes, move the object
                this.ActiveLocation += tranformation;
            }
            else
            {
                var message = "Object is off grid";
                Logger.Log(message);
                throw new OffGridException(message);
            }
        }

        /// <summary>
        /// Rover Rotate Left 
        /// </summary>
        public void RotateLeft()
        {
            var direction = new Direction(this.CurrentDirection.CurrentOrientation);
            direction.RotateLeft();
            this.CurrentDirection = direction; 
        }

        /// <summary>
        /// Rover Rotate Right
        /// </summary>
        public void RotateRight()
        {
            var direction = new Direction(this.CurrentDirection.CurrentOrientation);
            direction.RotateRight();
            this.CurrentDirection = direction;
        }

        /// <summary>
        /// Executes the list of ICommands queued in the command list
        /// </summary>
        public void RunCommands()
        {
            try
            {
                foreach (ICommand c in RoverCommandsList)
                {
                    c.Execute(this);
                }
            }
            catch (OffGridException e)
            {
                Logger.Log(e.Message);
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
        }
    }


}
