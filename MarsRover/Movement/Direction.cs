﻿using MarsRover.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MarsRover.Movement
{
    public class Direction
    {
        public Orientation CurrentOrientation { get; set; }

        public Direction(Orientation orientation)
        {
            CurrentOrientation = orientation;
        }

        public Direction(string s)
        {
            s = s.ToUpper();
            if (s == "N") CurrentOrientation = Orientation.N;
            if (s == "E") CurrentOrientation = Orientation.E;
            if (s == "W") CurrentOrientation = Orientation.W;
            if (s == "S") CurrentOrientation = Orientation.S;
        }

        public override string ToString()
        {
            return CurrentOrientation.ToString();
        }

        public void RotateLeft()
        {
            if (CurrentOrientation == Orientation.N)
            {
                CurrentOrientation = Orientation.W;
            }
            else if (CurrentOrientation == Orientation.E)
            {
                CurrentOrientation = Orientation.N;
            }
            else if (CurrentOrientation == Orientation.W)
            {
                CurrentOrientation = Orientation.S;
            }
            else if (CurrentOrientation == Orientation.S)
            {
                CurrentOrientation = Orientation.E;
            }
        }

        public void RotateRight()
        {
            if (CurrentOrientation == Orientation.N)
            {
                CurrentOrientation = Orientation.E;
            }
            else if (CurrentOrientation == Orientation.E)
            {
                CurrentOrientation = Orientation.S;
            }
            else if (CurrentOrientation == Orientation.W)
            {
                CurrentOrientation = Orientation.N;
            }
            else if (CurrentOrientation == Orientation.S)
            {
                CurrentOrientation = Orientation.W;
            }
        }

        public void Transform(ICommand command)
        {
            if (CurrentOrientation == Orientation.N)
            {
                if (command.GetType() == typeof(RotateLeftCommand))
                {
                    CurrentOrientation = Orientation.W;
                }
                if (command.GetType() == typeof(RotateRightCommand))
                {
                    CurrentOrientation = Orientation.E;
                }
            }
            else if (CurrentOrientation == Orientation.E)
            {
                if (command.GetType() == typeof(RotateLeftCommand))
                {
                    CurrentOrientation = Orientation.N;
                }
                if (command.GetType() == typeof(RotateRightCommand))
                {
                    CurrentOrientation = Orientation.S;
                }
            }
            else if (CurrentOrientation == Orientation.W)
            {
                if (command.GetType() == typeof(RotateLeftCommand))
                {
                    CurrentOrientation = Orientation.S;
                }
                if (command.GetType() == typeof(RotateRightCommand))
                {
                    CurrentOrientation = Orientation.N;
                }
            }
            else if (CurrentOrientation == Orientation.S)
            {
                if (command.GetType() == typeof(RotateLeftCommand))
                {
                    CurrentOrientation = Orientation.W;
                }
                if (command.GetType() == typeof(RotateRightCommand))
                {
                    CurrentOrientation = Orientation.E;
                }
            }
        }
    }
}
