﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover.Movement
{
    /// <summary>
    /// Basic Coordinates Class
    /// </summary>
    public class Coordinates
    {
        public int X { get; private set; }
        public int Y { get; private set; }


        public Coordinates()
        {
            this.X = 0;
            this.Y = 0;
        }

        public Coordinates(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Coordinates(int[] c)
        {
            this.X = c[0];
            this.Y = c[1];
        }

        /// <summary>
        /// Operator '+' allows to add 2 Coordinates
        /// </summary>
        /// <param name="v1">Coordinates v1</param>
        /// <param name="v2">Coordinates v2</param>
        /// <returns>The Coordinates v3 result</returns>
        public static Coordinates operator +(Coordinates v1, Coordinates v2)
        {
            var v3 = new Coordinates();
            v3.X = v1.X + v2.X;
            v3.Y = v1.Y + v2.Y;
            return v3;
        }

        /// <summary>
        /// Basic Movement based on direction and Grid Distance
        /// </summary>
        /// <param name="currentOrientation">The current orientation</param>
        /// <param name="gridDistance">The Grid Distance</param>
        /// <returns></returns>
        public Coordinates MoveTransform(Orientation currentOrientation, int gridDistance)
        {
            Coordinates directionVector = null;
            switch(currentOrientation)
            {
                //simple vector operations based on directions
                case Orientation.N:
                    directionVector = new Coordinates(0, 1 * gridDistance);
                    break;
                case Orientation.E:
                    directionVector = new Coordinates(1 * gridDistance, 0);
                    break;
                case Orientation.W:
                    directionVector = new Coordinates(-1 * gridDistance, 0);
                    break;
                case Orientation.S:
                    directionVector = new Coordinates(0, -1 * gridDistance);
                    break;
            }
            return directionVector;
        }

        public static Coordinates getOrigin()
        {
            return new Coordinates(0, 0);
        }

        public override string ToString()
        {
            return "(" + this.X.ToString() + "," + this.Y.ToString() + ")";
        }
    }
}
