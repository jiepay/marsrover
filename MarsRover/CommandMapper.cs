﻿using MarsRover.Commands;
using MarsRover.ErrorCheck;
using MarsRover.Movement;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    /// <summary>
    /// Essentially a validation, translation and mapper class
    /// </summary>
    public class CommandMapper: ICommandMapper
    {
        private Dictionary<string, ICommand> commandMappingTable;
        public Coordinates UpperBoundsCoordinates { get; private set; }
        public List<Rover> TempRoverList { get; set; }


        public CommandMapper(string commandString)
        {
            this.commandMappingTable = BuildCommandMappingTable();          
        }

        /// <summary>
        /// Validates an input block so that it can be used by the RoverBuilder in the RoverApp
        /// </summary>
        /// <param name="commandString">The Input block to feed the App</param>
        /// <returns>true for a valid input block</returns>
        public bool ValidateCommandSet(string commandString)
        {
            /** Command Input Rules **/
            /* FIRST LINE (1/2)
             * - Minimum grid position is always (0,0)
               - 1st line: Upper Boundary 
               - Assumption: the upper boundary values cannot be negative
            */
            var commandList = commandString.Split("\n");
            var result = IsCoordinateValid(commandList[0]);
            if (result==null)
            {
                Logger.Log("Invalid format for Upper bound");
                return false;
            }
            UpperBoundsCoordinates = result;

            /* SERIES OF PAIRS OF 2 LINES 
                - Line 1: Starting Position and Direction
                - Line 2: Command List
            */

            //since Rover Data is exactly 2 lines
            if ((commandList.Length-1) % 2 != 0 || commandList.Length <= 1)
            {
                Logger.Log("Invalid command list length");
                return false;
            }

            //to check the rover coordinates
            MarsPlateau.Upperbound = UpperBoundsCoordinates;
            MarsPlateau.Origin = Coordinates.getOrigin();
            //Assumes n number of rovers
            int x = 1;
            TempRoverList = new List<Rover>();
            while(x < commandList.Length)
            {
                var r = new Rover(string.Empty,null, null);//temp rover data
                try
                {
                    r = ExtractRoverLocationOrientation(commandList[x]); //rover data starting position and direction
                    if (!MarsPlateau.IsObjectInsidePlateau(r.ActiveLocation))
                    {
                        Logger.Log("Rover is off plateau after landing");
                        return false;
                    }
                    r.RoverCommandsList = TranslateStringPassedToCommandList(commandList[x + 1]);//creates rover list
                    if (r.RoverCommandsList==null)
                    {
                        throw new NullReferenceException("rover list contains invalid characters");
                    }
                    TempRoverList.Add(r);
                    x += 2; //next string set starts at [x+2]
                }
                catch(NullReferenceException e)
                {
                    Logger.Log(e.Message);
                    return false;
                }
                catch (Exception e)
                {
                    Logger.Log(e.Message);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Gets Rover Info
        /// </summary>
        /// <param name="roverLocationString">a string that contains rover information</param>
        /// <returns>returns a valid Rover object</returns>
        private Rover ExtractRoverLocationOrientation(string roverLocationString)
        {
            var commandList = roverLocationString.Split(" ");
            var xCoord = commandList[0];//rover x coordinate
            var yCoord = commandList[1];//rover y coordinate
            var currentOrientation = commandList[2];//current orientation
            try
            {
                var r = new Rover(
                    string.Empty,
                    new Coordinates(int.Parse(xCoord), int.Parse(yCoord)),
                    new Direction(currentOrientation));
                return r;
            }
            catch
            {
                Logger.Log("Invalid Rover Object");
                return null;
            }
        }

        /// <summary>
        /// Maps the command string to a list of ICommands
        /// </summary>
        /// <param name="commandString">the command string e.g. 'RMRM'</param>
        /// <returns>returns a list of ICommands</returns>
        private List<ICommand> TranslateStringPassedToCommandList(string commandString)
        {
            commandString = commandString.ToUpper();
            List<ICommand> commandList = new List<ICommand>();
            try
            {
                for(int x = 0; x < commandString.Length;x++)
                {
                    var s = commandString[x].ToString();
                    if (this.commandMappingTable.ContainsKey(s))
                    {
                        commandList.Add(this.commandMappingTable[s]);
                    }
                    else
                    {
                        throw new NullReferenceException("Invalid Character");
                    }
                }
                return commandList;
            }
            catch(Exception e)
            {
                Logger.Log(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Checks whether the upperbound string is valid
        /// </summary>
        /// <param name="upperBound">the upperbound string</param>
        /// <returns>a Coordinates</returns>
        private Coordinates IsCoordinateValid(string upperBound)
        {
            var numberList = upperBound.Split(" ");
            int[] c = new int[2];

            if (numberList.Length != 2)
                return null;
            try
            {               
                for(int i = 0; i < numberList.Length;i++)
                {
                    var num = int.Parse(numberList[i]);
                    c[i] = num;
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
                return null;
            }
         
            return new Coordinates(c);
        }

        /// <summary>
        /// Builds a map of string to ICommands
        /// </summary>
        /// <returns>returns that map</returns>
        private Dictionary<string, ICommand> BuildCommandMappingTable()
        {
            var commandList= new Dictionary<string, ICommand>();

            //create a fixed mapping of key strings to Command Objects
            commandList.Add("M", new MoveCommand());
            commandList.Add("L", new RotateLeftCommand());
            commandList.Add("R", new RotateRightCommand());

            return commandList;
        }
    }
}
