﻿using MarsRover.Movement;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover
{
    /// <summary>
    /// Defines the Grid for the Plateau
    /// </summary>
    public class MarsPlateau
    {
        /// <summary>
        /// Origin (0,0) - Default
        /// </summary>
        public static Coordinates Origin { get; set;  }
        /// <summary>
        /// Upperbound of the Grid
        /// </summary>
        public static Coordinates Upperbound { get; set; }

        /// <summary>
        /// Basic Grid Init()
        /// </summary>
        /// <param name="upperbound"></param>
        public MarsPlateau(Coordinates upperbound)
        {
            Origin = Coordinates.getOrigin();//default is (0,0)
            Upperbound = upperbound;
        }

        /// <summary>
        /// To check if the coordinates is inside the plateau
        /// </summary>
        /// <param name="objectCoords">Coordinates</param>
        /// <returns>true - means the object is inside</returns>
        public static bool IsObjectInsidePlateau(Coordinates objectCoords)
        {
            if (objectCoords.X > Upperbound.X ||
                objectCoords.Y > Upperbound.Y ||
                objectCoords.X < Origin.X ||
                objectCoords.Y < Origin.Y)
                return false;
            else
                return true;
        }
    }
}
