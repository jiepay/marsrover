﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MarsRover.ErrorCheck
{
    /// <summary>
    /// Basic Logger Class
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Stores the errors and messages in the app in a log file
        /// </summary>
        /// <param name="message"></param>
        public static void Log(string message)
        {
            using (var file = new StreamWriter(Environment.CurrentDirectory + "\\errors.csv", true))
            {
                var now = DateTime.Now;
                var dateFormatLog = now.Year + "-" + now.Month + "-" + now.Day + ","
                                     + now.Hour + ":" + now.Minute + ":" + now.Second;
                file.WriteLine(dateFormatLog + "," + message);
            }
        }
    }
}
