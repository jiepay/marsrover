﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarsRover.ErrorCheck
{
    public class OffGridException: Exception
    {
        public OffGridException() : base()
        {}

        public OffGridException(string message): base(message)
        {}
    }
}
