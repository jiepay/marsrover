using MarsRover;
using MarsRover.Commands;
using MarsRover.Movement;
using System;
using System.Collections.Generic;
using Xunit;

namespace XUnitTestRover
{
    public class UnitTest
    {
        [Fact]
        public void FormatStringTestEmpty()
        {
            var commandString = "";

            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);
            Assert.False(result);
        }

        [Fact]
        public void FormatStringUpperBoundsNoRover()
        {
            var commandString = "5 5";

            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);
            Assert.False(result);
        }

        [Theory]
        [InlineData("5 5\n1 x N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM")]
        [InlineData("5 x\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM")]
        [InlineData("5 5\n1 2 N\nLMLMLMLMM\n3 x E\nMMRMMRMRRM")]
        [InlineData("5 5\n1 2 N\nLMLMLxLMM\n3 3 E\nMMRMMRMRRM")]
        public void FormatStringFakeRoverInfo(string fakeInfo)
        {
            var commandString = fakeInfo;
                    
            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);
            Assert.False(result);
        }

        [Theory]
        [InlineData("5 5\n1 2 N\nLMLMLxLMM\nMMRMMRMRRM")]
        [InlineData("1 2 N\nLMLMLxLMM\n3 3 E")]
        [InlineData("1 2 N\nLMLMLxLMM\n3 3 E\nMMRMMRMRRM")]
        public void FormatStringWrongNumberOfLinesPerRoverInfo(string inconsistentNumberOfLines)
        {
            var commandString = inconsistentNumberOfLines;

            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);
            Assert.False(result);
        }

        [Fact]
        public void OffTheGridRover()
        {
            var commandString =
                     "5 5\n" +
                     "8 2 N\n" +
                     "LMLMLMLMM\n" +
                     "3 3 E\n" +
                     "MMRMMRMRRM";

            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);
            Assert.False(result);
        }

        [Theory]
        [InlineData("5 5\n2 2 N\nLMMMMM", "(0,2)")]
        [InlineData("5 5\n2 2 N\nRMMMMM", "(5,2)")]
        [InlineData("5 5\n2 2 N\nMMMMM", "(2,5)")]
        public void OffTheGridRoverAfterMovement(string commandString, string output)
        {
            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);

            var planetMars = new MarsPlateau(mapper.UpperBoundsCoordinates);
            var roverMission = new RoverBuilder(string.Empty,
                planetMars, mapper.TempRoverList);
            roverMission.RunCommandMapper();
            Assert.Equal(output, roverMission.Rovers[0].ActiveLocation.ToString());
        }

        [Theory]
        [InlineData("5 5\n1 3 N\nM","(1,4)")]
        [InlineData("5 5\n1 3 N\nLM", "(0,3)")]
        [InlineData("5 5\n1 3 N\nRM", "(2,3)")]
        [InlineData("5 5\n1 3 S\nM", "(1,2)")]
        [InlineData("5 5\n1 3 S\nLM", "(2,3)")]
        [InlineData("5 5\n1 3 S\nRM", "(0,3)")]
        [InlineData("5 5\n1 3 E\nM", "(2,3)")]
        [InlineData("5 5\n1 3 E\nLM", "(1,4)")]
        [InlineData("5 5\n1 3 E\nRM", "(1,2)")]
        [InlineData("5 5\n1 3 W\nM", "(0,3)")]
        [InlineData("5 5\n1 3 W\nLM", "(1,2)")]
        [InlineData("5 5\n1 3 W\nRM", "(1,4)")]
        public void RoverMovement(string commandString, string output)
        {         
            var mapper = new CommandMapper(commandString);
            var result = mapper.ValidateCommandSet(commandString);

            var planetMars = new MarsPlateau(mapper.UpperBoundsCoordinates);
            var roverMission = new RoverBuilder(string.Empty,
                planetMars, mapper.TempRoverList);
            roverMission.RunCommandMapper();

            Assert.Equal(output, roverMission.Rovers[0].ActiveLocation.ToString());
        }
    }
}
