﻿using MarsRover;
using MarsRover.Movement;
using System;

namespace RoverApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var commandString = "5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM";

            CommandMapper mapper = new CommandMapper(commandString);
            
            var result = mapper.ValidateCommandSet(commandString);

            if (!result)
                return;

            var planetMars = new MarsPlateau(mapper.UpperBoundsCoordinates);
            var roverMission = new RoverBuilder(string.Empty,
                planetMars, mapper.TempRoverList);
            roverMission.RunCommandMapper();
            Console.ReadLine();
        }
    }
}
