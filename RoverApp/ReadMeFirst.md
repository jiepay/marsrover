﻿Author: Jean Paul Fortuno
Date: 9th August 2018

Environment: 
- Visual Studio 2017 (Community)
- .net core 2.0
- xUnit
- .net framework 3.5
- unity

Assumptions:
- The command Input text is sent as a bulk and can only work that way
- The parser is based only on the text format provided\

5 5 (upper coords)
1 2 N (rover 1)
LMLMLMLMM (rover 1 movement)
3 3 E (rover 2)
MMRMMRMRRM (rover 2 movement)

- The format text is completed rejected if it does not pass the validation

- No collision detection was implemented. Rovers can overlap and run in series (not in parallel)

- Rovers are allowed to move and execute their commands but would stop on the upperbound plateau

The Projects (Each project has its own ReadMe)

RoverApp  - Console App to Start the Project
MarsRover - Library for the Mars Rover Problem
XUnitTestRover - The xUnit project to test the classes 

Possible Improvements
1. Config File for log file name
2. Merge Direction and Movement Classes into a more general Vector class with some extra Maths functions for advanced movements
3. Collision detection 
4. Possibly a better design for the logger