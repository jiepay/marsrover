﻿using System.Collections;
using System.Collections.Generic;
using MarsRover;
using MarsRover.Movement;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager gm;
    [Tooltip("Raw lines of commands")]
    public GameObject roverPrefab;
    public GameObject gridSquarePrefab;
    public string[] rawCommands; //Input Block equivalent to the console app
    List<RoverMovement> rovers = new List<RoverMovement>();
    List<char[]> roverCommandLists = new List<char[]>();
    int activeRover;

    List<Rover> Rovers;

    // Use this for initialization
    void Awake ()
    {
        //ensure only one instance
        if(gm)
        {
            Destroy(gameObject);
            return;
        }
        gm = this;

        string commandString = string.Empty;
        for (int x= 0;  x < rawCommands.Length; x++)
        {
            commandString = commandString + rawCommands[x];
            if (x + 1 < rawCommands.Length) commandString += "\n";
        }
        
        CommandMapper mapper = new CommandMapper(commandString);
        var result = mapper.ValidateCommandSet(commandString);

        var planetMars = new MarsPlateau(mapper.UpperBoundsCoordinates);
     
        //STEP 0: Set Grid upperbound, the grid will appear as a (x+1,y+1) grid though the size is (x,y). A rover moving to (5,1), will have its bottom-left tip on (5,1) even though it seems to exceed the grid graphically
        Vector2Int gridSize = new Vector2Int();
        gridSize.x = MarsPlateau.Upperbound.X;
        gridSize.y = MarsPlateau.Upperbound.Y;

        //STEP 1: draw the grid
        for(int x = 0; x <= gridSize.x; x++)
        {
            for(int y = 0; y <= gridSize.y; y++)
            {
                GameObject sq = Instantiate(gridSquarePrefab, new Vector3(x, y, 1), new Quaternion());
            }
        }

        Rovers = mapper.TempRoverList;

        //get rovers count
        int roversCount = (rawCommands.Length - 1) / 2;
        for(int i = 0; i < roversCount; i++)
        {
            //get rover starting position
            int x = Rovers[i].ActiveLocation.X;
            int y = Rovers[i].ActiveLocation.Y;
            //get starting direction
            string dir = rawCommands[(i * 2) + 1].Split(' ')[2];

            //deploy rover
            GameObject roverAsset = Instantiate(roverPrefab); //rover asset that corresponds to the game
            roverAsset.transform.position = new Vector3(x, y, 0);

            int rot = 0;
            if(Rovers[i].CurrentDirection.CurrentOrientation == Orientation.N)
            {
                rot = 0;
            }
            else if(Rovers[i].CurrentDirection.CurrentOrientation == Orientation.W)
            {
                rot = 90;
            }
            else if(Rovers[i].CurrentDirection.CurrentOrientation == Orientation.S)
            {
                rot = 180;
            }
            else if(Rovers[i].CurrentDirection.CurrentOrientation == Orientation.E)
            {
                rot = 270;
            }
            roverAsset.transform.eulerAngles = new Vector3(0, 0, rot);
            RoverMovement rm = roverAsset.GetComponent<RoverMovement>();
            rm.curRot = rot;
            rm.nextRot = rot;
            rm.curLoc = new Vector2Int(x, y);
            rm.nextLoc = new Vector2Int(x, y);
            rm.gridSize = gridSize;
            rm.CommandsList = Rovers[i].RoverCommandsList;

            roverCommandLists.Add(rawCommands[(i * 2) + 2].ToCharArray());//previous version where the commands were processed as chars
            rovers.Add(rm);
        }

        //begin processing the first rover
        if(rovers.Count > 0)
        {
            print(roverCommandLists[0]);
            //rovers[0].ProcessCommands(roverCommandLists[0]);
            rovers[0].ProcessICommands(Rovers[0].RoverCommandsList);
            activeRover = 0;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(activeRover >= rovers.Count)
        {
            return;
        }
        //check if active rover has finished processing
        RoverMovement rm = rovers[activeRover];
        if(!rm.isProcessing && rm.CommandsList.Count == 0)
        {
            activeRover++;
            if(rovers.Count == activeRover)
            {
                //no more rovers to process
                return;
            }
            rovers[activeRover].ProcessICommands(rovers[activeRover].CommandsList);
        }
	}
}
