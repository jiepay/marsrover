﻿using System.Collections;
using System.Collections.Generic;
using MarsRover;
using MarsRover.Commands;
using UnityEngine;

public class RoverMovement : MonoBehaviour {
    public List<char> CommandList = new List<char>();
    public List<ICommand> CommandsList = new List<ICommand>();

    [Tooltip("Current location of the rover")]
    public Vector2Int curLoc;
    [Tooltip("Next location of the rover")]
    public Vector2Int nextLoc;
    [Tooltip("Direction rover is facing: N = 0, S = 180, W = 90, E = 270")]
    public int curRot;
    [Tooltip("Direction rover will face next: N = 0, S = 180, W = 90, E = 270")]
    public int nextRot;
    public Vector2Int gridSize;
    [Tooltip("How long it takes to complete a command")]
    public float moveDuration;
    public bool isMoving;
    public float moveTimer;
    public bool isProcessing;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //if we're moving, update timer
        if(isMoving) {
            moveTimer += Time.deltaTime;
            
            if(moveTimer >= moveDuration) {
                //finish moving
                print("Finish moving");
                transform.position = new Vector3(nextLoc.x, nextLoc.y, transform.position.z);
                transform.eulerAngles = new Vector3(0, 0, nextRot);
                isMoving = false;
                curLoc = nextLoc;
                curRot = nextRot;
                //if we're running commands from a command list
                if(isProcessing) {
                    if(CommandsList.Count == 0) {
                        isProcessing = false;
                    }
                    else {
                        PerformIAction(CommandsList[0]);
                        CommandsList.RemoveAt(0);
                    }
                }
            }
            else {
                //move gradually
                Vector3 prevPos = new Vector3(curLoc.x, curLoc.y, transform.position.z);
                Vector3 nextPos = new Vector3(nextLoc.x, nextLoc.y, transform.position.z);
                transform.position = Vector3.Lerp(prevPos, nextPos, moveTimer / moveDuration);
                //rotate gradually
                transform.eulerAngles = new Vector3(
                    transform.eulerAngles.x,
                    transform.eulerAngles.y,
                    Mathf.LerpAngle(curRot, nextRot, moveTimer / moveDuration)
                );
            }
        }
	}

    public void Move()
    {
        if(curRot == 0) {
            //we're facing north
            nextLoc.y = Mathf.Clamp(curLoc.y + 1, 0, gridSize.y);
        }
        else if(curRot == 90) {
            //we're facing west
            nextLoc.x = Mathf.Clamp(curLoc.x - 1, 0, gridSize.x);
        }
        else if(curRot == 180) {
            //we're facing south
            nextLoc.y = Mathf.Clamp(curLoc.y - 1, 0, gridSize.y);
        }
        else if(curRot == 270) {
            //we're facing east
            nextLoc.x = Mathf.Clamp(curLoc.x + 1, 0, gridSize.x);
        }
        moveTimer = 0;
        isMoving = true;
    }

    public void RotateLeft() {
        nextRot = WrapAngle(curRot + 90);
        moveTimer = 0;
        isMoving = true;
    }

    public void RotateRight()
    {
        nextRot = WrapAngle(curRot - 90);
        moveTimer = 0;
        isMoving = true;
    }

    public void ProcessCommand(string command)
    {
        ProcessCommand(command[0]);
    }

    public void ProcessCommand(char command) {
        if(isProcessing) {
            CommandList.Add(command);
            return;
        }
        PerformAction(command);
    }

    public void ProcessICommand(ICommand command)
    {
        if(isProcessing) {
            CommandsList.Add(command);
            return;
        }
        PerformIAction(command);
    }

    public void PerformIAction(ICommand command)
    {
        if (command.GetType() == typeof(MoveCommand))
        {
            isProcessing = true;
            Move();
        }
        else if (command.GetType() == typeof(RotateLeftCommand))
        {
            isProcessing = true;
            RotateLeft();
        }
        else if (command.GetType() == typeof(RotateRightCommand))
        {
            isProcessing = true;
            RotateRight();
        }
    }

    void PerformAction(char command)
    {
        if(command.ToString().ToUpper() == "M")
        {
            isProcessing = true;
            Move();
        }
        else if(command.ToString().ToUpper() == "L")
        {
            isProcessing = true;
            RotateLeft();
        }
        else if(command.ToString().ToUpper() == "R")
        {
            isProcessing = true;
            RotateRight();
        }
    }

    public void ProcessCommands(char[] commands) {
        CommandList.AddRange(commands);
        if(!isProcessing)
        {
            ProcessCommand(CommandList[0]);
            CommandList.RemoveAt(0);
        }
    }

    public void ProcessICommands(List<ICommand> commands)
    {
        if (!isProcessing)
        {
            ProcessICommand(commands[0]);
            commands.RemoveAt(0);
        }
    }

    int WrapAngle(int a) {
        int result = a;

        while(result >= 360)
        {
            result -= 360;
        }
        while(result < 0)
        {
            result += 360;
        }

        return result;
    }
}
