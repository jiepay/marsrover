﻿An attempt to use Unity as UI for the Rover Project.

IMPORTANT: The MarsRover Project has been recompiled to .NET3.5 to suit the UNITY environment. It is essentially the same code with some minor modifications (.Split for example) so that it works with UNITY.

I am also assuming that you will install the latest Unity 2018.2.2f1 (64-bit) to test the visual Rover moving on Planet Mars in 2d